\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% gloria a dios...
		d2 a2 b1:m g1 a1 g2 a2 d1

		% por tu inmensa gloria...
		b2:m fis2 b1:m
		b2:m fis2 b1:m
		a1 d1 g2 e2:m fis1 fis1
		a1 d1 g2 e2:m fis1 fis2. a4

		% gloria a dios...
		d2 a2 b1:m g1 a1 g2 a2 d1

		% señor hijo único, jesucristo...
		b2:m fis2 b1:m
		b2:m fis2 b1:m
		a1 d1 g2 e2:m fis1
		a1 d1 g2 e2:m fis1
		a1 d1 g2 e2:m fis1 fis2. a4

		% sólo tú eres santo...
		d2 a2 b1:m g1 a1
		d2 a2 b1:m g2 a2 d1
		g1 a1 d2 g2 d1
	} % end chords
