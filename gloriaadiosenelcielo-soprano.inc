\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

                a' 4 b' a' fis' 8 e'  |
                d' 2 d'  |
                g' 4 a' b' a'  |
                e' 2 r4 e' 8 fis'  |
%% 5
                g' 4 d' 8 d' d' cis' b cis'  |
                d' 2 r4 d' 8 e'  |
                fis' 4 fis' fis' 8 e' d' cis'  |
                b 4 b r8 cis' d' e'  |
                fis' 4 fis' e' d' 8 cis'  |
%% 10
                b 4 b r2  |
                a 4 a e' cis'  |
                d' 4 d' 2 r4  |
                d' 2 e' 4 e'  |
                cis' 4 cis' 2. _~  |
%% 15
                cis' 2 r4 r8 b  |
                a 4 a e' cis' 8 cis'  |
                d' 2 r4 d'  |
                d' 4 d' e' 8 e' e' e'  |
                fis' 2 fis' _~  |
%% 20
                fis' 2 r  |
                a' 4 b' a' fis' 8 e'  |
                d' 2 d'  |
                g' 4 a' b' a'  |
                e' 2 r4 e' 8 fis'  |
%% 25
                g' 4 d' 8 d' d' cis' b cis'  |
                d' 2 r4 d' 8 e'  |
                fis' 8 fis' fis' fis' e' 4 d' 8 cis'  |
                b 4 b r d' 8 e'  |
                fis' 4 fis' e' d' 8 cis'  |
%% 30
                b 2 r  |
                a 2 e' 4 cis'  |
                d' 4 d' r d' 8 d'  |
                d' 8 d' d' d' e' 4 e' 8 e'  |
                cis' 4 cis' r r  |
%% 35
                a 4 a e' cis' 8 cis'  |
                d' 4 d' 2 r8 d'  |
                d' 4 d' e' e'  |
                cis' 8 cis' cis' 4 r a 8 a  |
                a 8 a a a e' 4 d' 8 cis'  |
%% 40
                d' 4 d' 2 r4  |
                d' 4 d' e' e' 8 e'  |
                fis' 2 fis' _~  |
                fis' 2 r  |
                a' 4 b' a' fis' 8 e'  |
%% 45
                d' 2 d'  |
                g' 4 a' b' a'  |
                e' 2 r4 fis' 8 g'  |
                a' 8 a' a' b' a' 4 fis' 8 e'  |
                d' 4 d' r d' 8 d'  |
%% 50
                d' 4 cis' b cis'  |
                d' 4 d' 2 r4  |
                g' 2. _( fis' 4 
                % warning: overlong bar truncated here |
                e' 2. d' 4 )  |
                d' 1 _~  |
%% 55
                d' 1  |
                \bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Glo -- "ria a" Dios en el cie -- lo
		"y en" la tie -- rra paz
		a los hom -- bres que a -- ma el Se -- ñor.

		Por "tu in" -- men -- sa glo -- ria "te a" -- la -- ba -- mos,
		te ben -- de -- ci -- mos, te  a -- do -- ra -- mos,
		te glo -- ri -- fi -- ca -- mos,
		te da -- mos gra -- cias, __
		Se -- ñor Dios, rey ce -- les -- tial,
		Dios Pa -- dre to -- do -- po -- de -- ro -- so. __

		Glo -- "ria a" Dios en el cie -- lo
		"y en" la tie -- rra paz
		a los hom -- bres que a -- ma el Se -- ñor.

		Se -- ñor Hi -- jo ú -- ni -- co, Je -- su -- cris -- to,
		Se -- ñor Dios, Cor -- de -- ro de Dios,
		Hi -- jo del Pa -- dre:
		tú que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad de no -- so -- tros
		"y a" -- tien -- de nues -- tra sú -- pli -- ca;
		tú "que es" -- tás a la de -- re -- cha del Pa -- dre,
		ten pie -- dad de no -- so -- tros. __

		Só -- lo tú e -- res San -- to,
		só -- lo tú, Se -- ñor,
		só -- lo tú, al -- tí -- si -- mo Je -- su -- cris -- to,
		en la glo -- ria de Dios Pa -- dre.
		A __ mén. __
	}
>>
